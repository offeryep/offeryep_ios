//
//  ShowLocationViewController.h
//
//  Created by Rahul Sharma on 3/11/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ShowLocationViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (strong,nonatomic) NSString *storeLocationStr;
@property (weak, nonatomic) IBOutlet UISegmentedControl *Segment;

@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
- (IBAction)shareBtncliked:(id)sender;
- (IBAction)backAction:(id)sender;


@end
