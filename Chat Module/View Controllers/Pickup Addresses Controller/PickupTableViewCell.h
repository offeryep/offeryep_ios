//
//  PickupTableViewCell.h
//
//  Created by Rahul Sharma on 3/16/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mainLbl;
@property (weak, nonatomic) IBOutlet UILabel *subLbl;

@end
