//
//  RestKitWebServiceHandler.h
//  RedBag
//
//  Created by Nabeel Gulzar on 12/28/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <RestKit/RestKit.h>

//#define BASE_URL_RESTKIT @"http://208.94.245.186"
//#define BASE_URL_RESTKIT @"http://159.203.143.251"
#define BASE_URL_RESTKIT @"http://159.203.143.251" //Live server URL
//#define BASE_URL_RESTKIT @"http://apis.shoppen-app.com" //Live server URL
@interface RestKitWebServiceHandler : NSObject
{
    NSArray * response;
}
+(id)sharedInstance;

-(void)composeRequestWithMethod:(NSString*)method paramas:(NSDictionary*)params onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock;
-(void)composeRequestWithMethodGET:(NSString*)method paramas:(NSDictionary*)params onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock;

@end
