//
//  ChatSuggesstionCollectionViewCell.h
//  Yelo
//
//  Created by 3Embed on 10/07/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatSuggesstionCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *chatSuggestionLabel;

@end
