//
//  DialCodeManager.h
//  Trustpals
//
//  Created by Rahul Sharma on 22/04/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VNHDialCode : NSObject

/**
 *  Method to get code based on Country code
 *
 *  @param countryCode Country code
 *
 *  @return DialCode
 */
+ (NSString *)getDialCodeForCountryCode:(NSString *)countryCode;

@end
