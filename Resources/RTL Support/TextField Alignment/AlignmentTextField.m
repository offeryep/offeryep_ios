//
//  AlignmentTextField.m
//  Jaiecom
//
//  Created by Rahulsharma on 03/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "AlignmentTextField.h"

@implementation AlignmentTextField

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setTextAlignmentForLeftAlignedTextField];
   
        
}

- (void)setTextAlignmentForLeftAlignedTextField {
    
    if([[RTL sharedInstance] isRTL]) {
            [self setTextAlignment:NSTextAlignmentRight];
    }
    else {
            [self setTextAlignment:NSTextAlignmentLeft];
    }
}



@end
