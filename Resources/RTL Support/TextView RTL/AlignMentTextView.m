//
//  AlignMentTextView.m


//  Created by Rahul Sharma on 28/12/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AlignMentTextView.h"

@implementation AlignmentTextView

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setTextAlignmentForLeftAlignedTextField];
    
    
}

- (void)setTextAlignmentForLeftAlignedTextField {
    
    if([[RTL sharedInstance] isRTL]) {
        [self setTextAlignment:NSTextAlignmentRight];
    }
    else {
        [self setTextAlignment:NSTextAlignmentLeft];
    }
}
@end
