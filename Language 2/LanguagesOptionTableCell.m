//
//  LanguagesOptionTableCell.m
//  Offeryep
//
//  Created by 3Embed Software Technologies Pvt LTd on 08/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

#import "LanguagesOptionTableCell.h"

@implementation LanguagesOptionTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
