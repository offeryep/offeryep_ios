//
//  LanguagesOptionTableCell.h
//  Offeryep
//
//  Created by 3Embed Software Technologies Pvt LTd on 08/01/19.
//  Copyright © 2019 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LanguagesOptionTableCell : UITableViewCell
- (void)setLanguageName:(NSString *)name andIsSelected:(BOOL)selected;
@property (nonatomic, strong)NSString *previousSelection;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;

@end

NS_ASSUME_NONNULL_END
