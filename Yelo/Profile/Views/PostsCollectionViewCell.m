//
//  PostsCollectionViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PostsCollectionViewCell.h"

@implementation PostsCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderWidth= 0.5f;
    self.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.contentView.backgroundColor =[UIColor clearColor];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.postImageView.image = nil ;
}



/**
 Set Products on homescreen.
 
 @param product productModel.
 */
-(void)setProducts:(ProductDetails *)product;
{
    
//    if(product.isPromoted)
//    {
//        self.featuredView.hidden = NO ;
//    }
//    else
//    {
//        self.featuredView.hidden = YES ;
//    }
//
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    [self.postImageView setImageWithURL:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    // request image
    
    NSString *productMainUrl =  [product.mainUrl stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit,h_500,q_40,w_500/"];
    
    if([manager diskImageExistsForURL:[NSURL URLWithString:productMainUrl]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.postImageView setImage: [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:productMainUrl]];
        });
    }
    else
    {
        [self.postImageView setImageWithURL:[NSURL URLWithString:productMainUrl] placeholderImage:[UIImage imageNamed:@""] options:0 completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
            dispatch_async(dispatch_get_main_queue(),^{
                [UIView transitionWithView:self.postImageView duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    [self.postImageView setImage:image];
                }completion:NULL];
            });
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
        
    }
}


@end
