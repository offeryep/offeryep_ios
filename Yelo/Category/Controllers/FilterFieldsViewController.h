//
//  FilterFieldsViewController.h
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filter.h"

@interface FilterFieldsViewController : UIViewController

@property(retain,nonatomic)NSMutableDictionary *filterKeyValueDict;
@property(strong,nonatomic)NSArray *filter;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
@property(nonatomic,weak)NSString *appendNavTitle;
@property(nonatomic,weak)NSString *subCategory;

@property (weak, nonatomic) IBOutlet UIButton *backButtonOutlet;


@property (weak, nonatomic) IBOutlet UIButton *doneButtonOutlet;

- (IBAction)donebuttonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;

@end
