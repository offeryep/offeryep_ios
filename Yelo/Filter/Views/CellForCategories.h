//
//  CellForCategories.h

//
//  Created by Rahul Sharma on 02/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewController.h"

@interface CellForCategories : UITableViewCell <UICollectionViewDelegate , UICollectionViewDataSource, WebServiceHandlerDelegate>

@property(weak,nonatomic)FilterViewController *filterVC;
@property(weak,nonatomic)FilterListings *filterListingsRef;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong)NSArray *arrayOfCategoryList;


@end
