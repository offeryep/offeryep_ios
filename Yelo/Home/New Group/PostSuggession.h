//
//  PostSuggession.h
//  Tac Traderz
//
//  Created by 3Embed on 07/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostSuggession : NSObject

@property(nonatomic,readonly)NSString *productName;
@property(nonatomic,readonly)NSString *postId;


-(instancetype)initWithDictionary:(NSDictionary *)response;
+(NSMutableArray *) arrayOfPostSuggessions :(NSArray *)responseData;
@end
