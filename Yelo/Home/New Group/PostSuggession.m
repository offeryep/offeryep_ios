//
//  PostSuggession.m
//  Tac Traderz
//
//  Created by 3Embed on 07/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PostSuggession.h"

@implementation PostSuggession

-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    if (!self) { return nil; }
    _productName = flStrForObj(response[@"swapTitle"]);
    if(_productName.length == 0)
    {
        _productName = flStrForObj(response[@"productName"]);
    }
    
    _postId = flStrForObj(response[@"swapPostId"]);
    
    if(_postId.length == 0)
    {
        _postId = flStrForObj(response[@"postId"]);
    }

    return self;
}

+(NSMutableArray *) arrayOfPostSuggessions :(NSArray *)responseData
{
    NSMutableArray *suggessions = [[NSMutableArray alloc]init];
    for(NSDictionary *postDic in responseData) {
        
        PostSuggession *post = [[PostSuggession alloc]initWithDictionary:postDic] ;
        [suggessions addObject:post];
    }
    
    return suggessions ;
}

@end
