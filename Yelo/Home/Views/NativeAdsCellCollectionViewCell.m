//
//  NativeAdsCellCollectionViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 11/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "NativeAdsCellCollectionViewCell.h"

@implementation NativeAdsCellCollectionViewCell


-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.masksToBounds = NO ;
    self.layer.shadowColor = mDividerColor.CGColor;
    self.layer.shadowOffset = CGSizeZero ;
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 2.0 ;
    self.contentView.backgroundColor =[UIColor whiteColor];
    self.layer.borderColor = mDividerColor.CGColor ;
    [self layoutIfNeeded];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.postedImageOutlet.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight ) cornerRadii:CGSizeMake(3.0,3.0)];
//    
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//    maskLayer.frame = self.postedImageOutlet.bounds;
//    maskLayer.path  = maskPath.CGPath;
//    self.postedImageOutlet.layer.mask = maskLayer;
}


-(void)setNativeAdFor :(FBNativeAd *)nativeAd
{
    [nativeAd.icon loadImageAsyncWithBlock:^(UIImage *image) {
        self.adIconImageView.image = image;
    }];
    self.adStatusLabel.text = @"";
    
    // Create native UI using the ad metadata.
    [self.adCoverMediaView setNativeAd:nativeAd];
    // Render native ads onto UIView
    self.adTitleLabel.text = nativeAd.title;
    self.adBodyLabel.text = nativeAd.body;
    self.adSocialContextLabel.text = nativeAd.socialContext;
    self.sponsoredLabel.text = @"Sponsored";
    
    [self.adCallToActionButton setTitle:nativeAd.callToAction
                                     forState:UIControlStateNormal];
    
    
    // Wire up UIView with the native ad; the whole UIView will be clickable.
    [nativeAd registerViewForInteraction:self.adUIView
                           withViewController:self.vcRefrence];
    
    self.adChoicesView.nativeAd = nativeAd;
    self.adChoicesView.corner = UIRectCornerTopRight;
}
@end
