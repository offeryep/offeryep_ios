//
//  CellForFilteredItem.h

//
//  Created by Rahul Sharma on 06/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedFilters.h"

@interface CellForFilteredItem : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *filterName;

@property (weak, nonatomic) IBOutlet UIImageView *filterImage;

@property (weak, nonatomic) IBOutlet UIButton *removeFilterButton;

-(void)setValueForSelectedFilters :(SelectedFilters *)filter;

@end
