//
//  CategoryCollectionViewCell.h
//  CollegeStax
//
//  Created by Rahul Sharma on 18/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"
#import "HomeScreenController.h"
#import "Category.h"

@interface CategoryCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong)Category *category;

@property(strong,nonatomic) HomeScreenController *homeVC;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;
@property (weak, nonatomic) IBOutlet UILabel *categoryName;


-(void)setCategories :(NSArray *)category forIndexPath:(NSIndexPath *)indexPath;

@end
