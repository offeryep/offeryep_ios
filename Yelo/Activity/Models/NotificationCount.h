//
//  Notification.h
//  Snapflipp
//
//  Created by Rahul Sharma on 29/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationCount : NSObject
@property(nonatomic,strong)NSString *notificationsCount;
+(instancetype)sharedInstance;

+(instancetype)initWithCount:(NSString *)labelCount ;
@end

