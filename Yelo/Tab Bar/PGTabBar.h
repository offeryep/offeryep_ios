//
//  TabBar.h

//
//  Created by Rahul Sharma on 2/26/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PGTabBar : UITabBarController
-(void)selectHomeScreen;

@property (nonatomic, getter=isTabBarHidden) BOOL tabBarHidden;

- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated;
+(instancetype)sharedInstance;

@end
