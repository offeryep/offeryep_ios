//
//  AddImagesTableViewCell.h
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"


@interface AddImagesTableViewCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>
{
     NSMutableArray  *arrayOfContainerWidths, *arrayOfContainerHeights;
}

@property (nonatomic,strong) Listings *listing ;
@property (nonatomic,strong) PostListingsViewController *referenceVC;
@property (nonatomic,strong) NSMutableArray *arrayOfImagePaths;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewForImages;

@property (weak, nonatomic) IBOutlet AlignmentTextField *titleTextField;
@property (weak, nonatomic) IBOutlet AlignmentOfLabel *labelForCount;


@end
