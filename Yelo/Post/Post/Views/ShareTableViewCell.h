//
//  ShareTableViewCell.h
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"

@interface ShareTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SwitchRTL *switchOutlet;

@property (nonatomic,strong) Listings *listing ;
@property (strong,nonatomic) PostListingsViewController *refrenceVC ;
@property (weak, nonatomic) IBOutlet UIButton *shareMediaButtonOutletForTitle;

-(void)setMediaTitle :(NSString *)title mediaIcon_Off :(NSString *)mediaIconOff andMediaIcon_On :(NSString *)mediaIconOn;

- (IBAction)switchAction:(id)sender;


@end
