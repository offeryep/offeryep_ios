//
//  LibPhotosCollectionViewCell.h

//
//  Created by Rahul Sharma on 18/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LibPhotosCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageViewForLib;
@property (strong, nonatomic) IBOutlet UIImageView *check_mark_image;


@end
