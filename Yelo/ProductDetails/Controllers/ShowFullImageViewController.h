//
//  ShowFullImageViewController.h

//
//  Created by Rahul Sharma on 21/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowFullImageViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate>
{
    CGFloat xOrigin;
    UIScrollView *scrollView;
    UIImageView *imageView;
    UIPageControl *pageControl;
}
@property (strong,nonatomic)NSString *TaggedProducts,*taggedCoordinates;
@property (strong,nonatomic)NSArray *arrayOfImagesURL;
@property  NSInteger imageCount ,imageTag;

@end
