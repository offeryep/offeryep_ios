//
//  PropertiesCollectionViewCell.m
//  CollegeStax
//
//  Created by 3Embed on 12/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PropertiesCollectionViewCell.h"

@implementation PropertiesCollectionViewCell

-(void)SetLeftProductProperties :(Filter *)filter
{
    self.rightPropertyTitle.hidden = self.rightPropertyValue.hidden = YES;
    
    self.leftPropertyTitle.hidden = self.leftPropertyValue.hidden = NO;
    
    self.leftPropertyTitle.text = filter.fieldName;
    self.leftPropertyValue.text = filter.values;

}

-(void)SetRightProductProperties :(Filter *)filter
{
    self.leftPropertyTitle.hidden = self.leftPropertyValue.hidden = YES;
    
    self.rightPropertyTitle.hidden = self.rightPropertyValue.hidden = NO;
    self.rightPropertyTitle.text = filter.fieldName;
    self.rightPropertyValue.text = filter.values;
    
}


@end
