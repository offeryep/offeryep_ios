//
//  PropertiesCollectionViewCell.h
//  CollegeStax
//
//  Created by 3Embed on 12/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filter.h"

@interface PropertiesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftPropertyTitle;
@property (weak, nonatomic) IBOutlet UILabel *leftPropertyValue;
@property (weak, nonatomic) IBOutlet UILabel *rightPropertyTitle;

@property (weak, nonatomic) IBOutlet UILabel *rightPropertyValue;

-(void)SetLeftProductProperties :(Filter *)filter;

-(void)SetRightProductProperties :(Filter *)filter;
@end
